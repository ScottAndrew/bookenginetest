//
//  main.m
//  BookEngineTest
//
//  Created by Scott A Andrew on 7/14/14.
//  Copyright (c) 2014 New Wave Digital Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
